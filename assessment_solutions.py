import numpy as np
import pandas as pd
from scipy.stats import ttest_ind
import seaborn as sns
import matplotlib.pyplot as plt
import time

# Load the original dataset
original_data = pd.read_csv("laterite_mobilemoney_data.csv")

print("")
print("Program is Running. Please Wait a little...")
print("----------------------------------------------------------------")
print("")


##############################################################################################################################################
# SOLUTION TO QUESTION 1
# Formatting the data so that there is only one observation per perticipant
print("QUESTION 1")
print("-------------")

# group rows by hhid then aggregate into lists of strings
merged_data=original_data.groupby('hhid', as_index=False).agg(lambda x: ', '.join(set(x.astype(str))))

# Restore the numbers of accounts into the right form since the numbers were concatenated as is
merged_data.account_num.replace(['2, 1', '2, 1, 3','4, 2, 1, 3'],['2','3','4'], inplace=True)

merged_data.to_csv("merged_data.csv")
print("Merged Data has been saved as 'merged_data.csv'")
print("")
print("----------------------------------------------------------------")
print("")
##############################################################################################################################################

# SOLUTION TO QUESTION 2

print("QUESTION 2")
print("-------------")

# Deep Copy table from Question 1
extended_table = merged_data.copy(deep=True)

# Not registered for any type of financial account
extended_table["financially_excluded"] = \
np.where(extended_table['account_type']=="None", 'yes', 'no')

# Registered to at least mobile money or online bank account
extended_table["digital_financial_included"] = \
np.where(extended_table['account_type'].str.contains("Mobile Money|Bank Accout",regex=True), 'yes', 'no')


extended_table.to_csv("extended_table.csv")

print(extended_table)
print("Extended Table with 'financially_excluded' and 'digital_financial_included' columns has been saved as 'extended_table.csv'")
print("")
print("----------------------------------------------------------------")
print("")
##############################################################################################################################################

# SOLUTION TO QUESTION 3
print("QUESTION 3")
print("-------------")

# Calculate Total Number of Customers using Mobile Money
MobileMoneyCustomers_DF=extended_table['account_type'].str.contains("Mobile Money")
MobileMoneyCustomers = len(MobileMoneyCustomers_DF[MobileMoneyCustomers_DF == True].index)

print("Total Customers with Mobile Money: ",MobileMoneyCustomers)
#----------------------------------------------------------------------------
# Dataframe containing booleans satisfying the condition that
# Mobile money is provided by Company_A
companyA_MobileMoney_DF=extended_table['account_type'].str.contains("Mobile Money") & \
extended_table['mm_account_telco'].str.contains("Company_A")

# Count number of True in series
companyA_MobileMoney = len(companyA_MobileMoney_DF[companyA_MobileMoney_DF == True].index)

print("Number of Customers using Mobile Money by Company_A:",companyA_MobileMoney)

#----------------------------------------------------------------------------
# Dataframe containing booleans satisfying the condition that
# Mobile money is provided by Company_B
companyB_MobileMoney_DF=extended_table['account_type'].str.contains("Mobile Money") & \
extended_table['mm_account_telco'].str.contains("Company_B")

# Count number of True in series
companyB_MobileMoney = len(companyB_MobileMoney_DF[companyB_MobileMoney_DF == True].index)

print("Number of Customers using Mobile Money by Company_B:",companyB_MobileMoney)

#----------------------------------------------------------------------------
# Dataframe containing booleans satisfying the condition that
# Mobile money is provided by Company_C
companyC_MobileMoney_DF=extended_table['account_type'].str.contains("Mobile Money") & \
extended_table['mm_account_telco'].str.contains("Company_C")

# Count number of True in series
companyC_MobileMoney = len(companyC_MobileMoney_DF[companyC_MobileMoney_DF == True].index)

print("Number of Customers using Mobile Money by Company_C:",companyC_MobileMoney)

#----------------------------------------------------------------------------

# Calculate Shares
MobileMoneyTotal = companyA_MobileMoney+companyB_MobileMoney+companyC_MobileMoney
print("Company_A's share in providing Mobile Money: ",(companyA_MobileMoney/MobileMoneyTotal)*100)
print("Company_B's share in providing Mobile Money: ",(companyB_MobileMoney/MobileMoneyTotal)*100)
print("Company_C's share in providing Mobile Money: ",(companyC_MobileMoney/MobileMoneyTotal)*100)

print("")
print("----------------------------------------------------------------")
print("")

##############################################################################################################################################

# SOLUTION TO QUESTION 4
# v240:  Has a transaction ever failed to go through? : Yes / No
# urban : Urban / Rural
print("QUESTION 4")
print("-------------")
#----------------------------------------------------------------------------
# Dataframe containing booleans satisfying the conditio that
# a mobile money transaction failed

fails_DF=extended_table['v240'].str.contains("yes")

# Count number of True in series
totalFails = len(fails_DF[fails_DF == True].index)

print("Total fails:",totalFails)
#----------------------------------------------------------------------------
# Dataframe containing booleans satisfying the condition that
# Mobile money transaction failed and the customer is from urban areas
urbanFails_DF=extended_table['urban'].str.contains("Urban") & \
extended_table['v240'].str.contains("yes")

# Count number of True in series
urbanFails = len(urbanFails_DF[urbanFails_DF == True].index)

print("Number of Fails in Urban Areas:",urbanFails)

#----------------------------------------------------------------------------

# Dataframe containing booleans satisfying the condition that
# Mobile money transaction failed and the customer is from rural areas
ruralFails_DF=extended_table['urban'].str.contains("Rural") & \
extended_table['v240'].str.contains("yes")

# Count number of True in series
ruralFails = len(ruralFails_DF[ruralFails_DF == True].index)

print("Number of Fails in Rural Areas:",ruralFails)

#----------------------------------------------------------------------------
print("Share in Urban Areas Fails:",(urbanFails/totalFails)*100)
print("Share in Ruraö Areas Fails:",(ruralFails/totalFails)*100)


#Statistical Significance
# Convert boolens for fails in urban and rural areas into
# numerical values
urbanFails_DF =  pd.DataFrame(urbanFails_DF)
urbanFails_DF.columns=["urban"]
urbanFails_NUM=urbanFails_DF.urban.astype('category').cat.codes

ruralFails_DF =  pd.DataFrame(ruralFails_DF)
ruralFails_DF.columns=["rural"]
ruralFails_NUM =ruralFails_DF.rural.astype('category').cat.codes

# compare samples
# If we fail to reject this hypothesis, it means that there is no 
# significant difference between the means.
stat, p = ttest_ind(urbanFails_NUM,ruralFails_NUM)

print('Statistics=%.3f, p=%.12f' % (stat, p))

# interpret
alpha = 0.05
if p > alpha:
    print('Same distributions (Accept H0). Not significant')
else:
    print('Different distributions (reject H0). Significant difference exists.')


print("")
print("----------------------------------------------------------------")
print("")
    
##############################################################################################################################################

# SOLUTION TO QUESTION 5
print("QUESTION 5")
print("-------------")

# Deep Copy table from Question 2
extended_table_NM = extended_table.copy(deep=True)


# Convert categorical values to numerical
# so that that table can be represented on a heatmap
cols = extended_table_NM.columns
for col in extended_table_NM.columns: 
    extended_table_NM[col] = extended_table_NM[col].astype('category').cat.codes


# Save the numerical table
extended_table_NM.to_csv("extended_table_NM.csv")


corr = extended_table_NM.corr()

# Save the correlations table
corr.to_csv("correlations.csv")

sns.heatmap(corr, 
            xticklabels=corr.columns.values,
            yticklabels=corr.columns.values)


print("Extended Table with Numerical Values has been saved as 'extended_table_NM.csv'")
print("Correlation Table has been saved as 'correlations.csv'")
print("")
print("----------------------------------------------------------------")
print("")

plt.title("CORRELATION HEATMAP FOR THE DATA")
time.sleep(1)
plt.show()